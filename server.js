const express=require('express');
var session = require('express-session');
const bodyParser=require('body-parser');
const tinify = require("tinify");
tinify.key = "c8VqQnwlrYBfDlnbLc0kMqZC5gpGGsd0";
// const cookieParser=require('cookie-parser');
// express app
const app=express();
// parse requests of content-type -application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended:true}));
// parse request of content-type - application/json
app.use(bodyParser.json());
//express sessions
app.use(session({
    secret: '5e525821a8c819873ce2fda3acd0c918',
    resave: true,
    saveUninitialized: true
}));
// db configuration
const dbConfig=require('./config/database.config.js');
const mongoose=require('mongoose');
// connecting to the database
mongoose.connect(dbConfig.url,{
    useNewUrlParser:true
}).then(()=>{
    console.log("DB Connection Successful");
}).catch(err=>{
    console.log("Could not connect to the database. Exiting now", err);
    process.exit();
});
//website middlewares
app.use("/css", express.static(__dirname+"/app/views/css"));
app.use("/js", express.static(__dirname+"/app/views/js"));
app.use("/assets", express.static(__dirname+"/app/views/assets"));
// website routes
app.get("/", (req, res)=>{
   res.sendFile(__dirname+"/app/views/index.html");
});
app.get("/event/:serviceID", (req, res)=>{
    res.sendFile(__dirname+"/app/views/event.html");
})
app.get("/GalleryHome", (req, res)=>{
    res.sendFile(__dirname+"/app/views/gallery_home.html");
});
app.get("/GalleryContent/:galleryID", (req, res)=>{
    res.sendFile(__dirname+"/app/views/gallery_content.html");
});
app.get("/AboutUs", (req, res)=>{
    res.sendFile(__dirname+"/app/views/about.html");
});

// admin middleware definition
app.use("/static", express.static(__dirname+"/app/views/admin/static"));
app.use("/appimages", express.static(__dirname+"/app/images"));
app.use("/galleryimages", express.static(__dirname+"/app/images/gallery"));

// admin routes
app.get("/master", (req, res)=>{
    res.sendFile(__dirname+"/app/views/admin/login.html");
});
app.get("/dashboard", (req, res)=>{
    checkLoginSession(req, res, "dashboard.html");
});
app.get("/gallery-content", (req, res)=>{
    checkLoginSession(req, res, "gallery.html");
});
app.get("/aboutus-content", (req, res)=>{
    checkLoginSession(req, res, "about.html");
});
app.get("/feedback-content", (req, res)=>{
    checkLoginSession(req, res, "feedbacks.html");
});
app.get("/intro-content", (req, res)=>{
    checkLoginSession(req, res, "intro-content.html");
});
app.get("/settings", (req, res)=>{
    checkLoginSession(req, res, "settings.html");
});
checkLoginSession=(req, res, url)=>{
    if(req.session.loggedin){
        res.sendFile(__dirname+"/app/views/admin/"+url);
    }else{
        res.redirect("/master");
    }
}
app.get("/logout", (req, res)=>{
    if(req.session.loggedin){
        req.session.loggedin=false;
        res.redirect("/master");
    }
});

// api routes
require('./app/routes/services.routes.js')(app);
require('./app/routes/gallery.routes.js')(app);
require('./app/routes/users.routes.js')(app);
require('./app/routes/aboutus.routes.js')(app);
require('./app/routes/feedbacks.routes.js')(app);
require('./app/routes/intro.routes.js')(app);
require('./app/routes/query.routes.js')(app);
// require('./app/routes/file.routes.js')(app);

// listen for requests
app.listen(8000, ()=>{
    console.log("Server started at port 8000");
});