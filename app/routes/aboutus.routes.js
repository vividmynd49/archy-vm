module.exports = (app) => {
    const content = require("../controllers/aboutus.controllers.js");

    app.post("/addaboutus", content.create);
    app.get("/getaboutus", content.findAll);
}