module.exports=(app)=>{
    const users=require("../controllers/users.controllers.js");

    // login a user
    app.post('/login', users.login);

    app.put('/update-login', users.updateLogin);

    // register a user
    // app.post('/register', users.register);
}