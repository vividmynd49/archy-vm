module.exports = (app) => {
    const files = require("../controllers/file.controllers.js");
    const multer = require('multer');

    function generate_random_string() {
        let random_string = '';
        let random_ascii;
        for (let i = 0; i < 10; i++) {
            random_ascii = Math.floor((Math.random() * 25) + 97);
            random_string += String.fromCharCode(random_ascii)
        }
        return random_string;
    }
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './app/images/')
        },
        filename: function (req, file, cb) {
            cb(null, generate_random_string() + ".jpg") //Appending .jpg
        }
    });
    const upload = multer({
        storage: storage
    });

    app.post("/fileupload", upload.array('edit_service_image', 10), files.upload);
}