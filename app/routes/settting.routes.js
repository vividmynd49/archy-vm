module.exports = (app) => {
    const settings = require("../controllers/setting.controllers.js");
    app.post("/addsetting", settings.create);
    app.get("/getsetting", settings.findAll);
    app.put("/updatesetting", settings.modifyOne);
}