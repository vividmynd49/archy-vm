module.exports=(app)=>{
    const services=require("../controllers/services.controllers.js");
    const multer=require('multer');
    function generate_random_string(){
      let random_string = '';
      let random_ascii;
      for(let i = 0; i < 10; i++) {
          random_ascii = Math.floor((Math.random() * 25) + 97);
          random_string += String.fromCharCode(random_ascii)
      }
      return random_string;
  }
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, './app/images/')
        },
        filename: function (req, file, cb) {
          cb(null,  generate_random_string()+".jpg") //Appending .jpg
        }
    });
 
    const upload=multer({storage: storage});
    // create a new service
    app.post('/services', upload.array('service_image', 10),services.create);

    // retrieve all services
    app.get('/services', services.findAll);

    // retrieve a single sesrvice with service id
    app.get('/services/:serviceId', services.findOne);

    // update a service with service id
    app.post('/updateService', upload.any(), services.update);

    // delete a service with service id
    app.delete('/services/:serviceId', services.delete);
}