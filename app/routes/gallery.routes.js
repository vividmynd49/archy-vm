module.exports=(app)=>{
    const gallery=require("../controllers/gallery.controllers.js");
    const multer=require('multer');
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, './app/images/gallery/')
        },
        filename: function (req, file, cb) {
          cb(null, file.originalname) //Appending .jpg
        }
    });
    const upload=multer({storage: storage});
    
    // create a new service
    app.post('/gallery', upload.array('story_images', 10),gallery.create);

    // retrieve all services
    app.get('/gallery', gallery.findAll);

    // retrieve a single sesrvice with service id
    app.get('/gallery/:galleryId', gallery.findOne);

    // update a service with service id
    app.put('/gallery/:galleryId', gallery.update);

    // delete a service with service id
    app.delete('/gallery/:galleryId', gallery.delete);
}