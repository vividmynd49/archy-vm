module.exports = (app) => {
    const intro = require("../controllers/intro.controllers.js");
    app.post("/addintro", intro.create);
    app.get("/getintro", intro.findAll);
    app.put("/updateintro", intro.modifyOne);
}