const Gallery = require('../models/gallery.model.js');
const fs=require('fs');
const tinify = require("tinify");
tinify.key = "c8VqQnwlrYBfDlnbLc0kMqZC5gpGGsd0";
//create and save a new service
exports.create = (req, res) => {
    req.files.forEach(function(file) {
        // Your logic with tinify here.
        var source = tinify.fromFile(file.path);
        source.toFile(file.path + "_optimized.jpg");
     });
    // validate request
    var imageObj = new Array();
    for (var i = 0; i < req.files.length; i++) {
        imageObj.push(req.files[i].originalname);
    }
    // creating a service
    const gallery = new Gallery({
        title: req.body.story_title || "Untitled Content",
        category: req.body.story_category,
        date: req.body.story_date,
        content: req.body.story_content,
        galleryID: "ARCHY-GAL-" + Math.floor(Math.random()),
        images: imageObj
    });
    // save content in the database
    gallery.save()
        .then(data => {
            res.redirect("/dashboard");
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred"
            });
        });
};

// retrieve services
exports.findAll = (req, res) => {
    Gallery.find()
        .then(story => {
            res.send(story);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred."
            });
        });
};

// finds a single service with id
exports.findOne = (req, res) => {
    Gallery.findById(req.params.galleryId)
        .then(story => {
            if (!story) {
                return res.status(404).send({
                    message: "No service found with id:" + req.params.galleryId
                });
            }
            res.send(story);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "No service found with id: " + req.params.galleryId
                });
            }
            return res.status(500).send({
                message: "Error retrieving service with id: " + req.params.galleryId
            });
        });

        console.log(__dirname);
};

//deletes a service
exports.delete = (req, res) => {
    var galleryID = req.params.galleryId;
    Gallery.deleteOne({
        _id: galleryID
    }, function (err, res) {});

    res.json({
        success: galleryID,
        status: '1 record deleted'
    });
};

//updates the service
exports.update = (req, res) => {
    if (!req.body.content) {
        return res.status(400).send({
            message: "Contents cannot be empty"
        });
    }
    let sid = req.params.galleryId;
    if(req.body.fordeletion.length>0){
        req.body.fordeletion.forEach(function(file){
            fs.unlinkSync('C:/xampp/htdocs/archyevents/app/images/gallery/'+file, (err) => {
                if (err) throw err;
                console.log(file+' was deleted');
              });
        });
    }
    Gallery.updateOne({
            _id: sid
        }, {
            title: req.body.title,
            content: req.body.content,
            images: req.body.images
        }, {
            upsert: true
        },
        function (err, stat) {
            if (err)
                return err.message;
            return res.json("record updated");
    });
}