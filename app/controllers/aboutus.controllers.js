const About = require('../models/aboutus.model.js');

//create and save a new service
exports.create = (req, res) => {
    // validate request
    // console.log(req.body.content);
    if (!req.body.content) {
        return res.status(400).send({
            message: "Contents cannot be empty"
        });
    }
    // creating an update
    const aboutus = new About({
        content: req.body.content
    });
    // save content in the database
    aboutus.save().then(data => {
            res.send("200", "OK");
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred"
        });
    });
};

// retrieve services
exports.findAll = (req, res) => {
    About.find()
        .then(content => {
            res.send(content);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred."
            });
        });
};