const Login = require('../models/users.model.js');
const bcrypt = require('bcrypt');
// login
exports.login = (req, res) => {
    var username = req.body.username;
    Login.findOne({
            'username': username
        })
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "No such user exists. Please try again."
                });
            }
            if (user) {
                bcrypt.compare(req.body.password, user.password, function (err, stat) {
                    if (stat) {
                        req.session.loggedin = true;
                        req.session.user = username;
                        res.send(200, "OK");
                    } else {
                        res.send(400, "Access denied");
                    }
                }); 

            } else
            res.send(400, "Bad request");
        }).catch(err => {
            if (err.kind == 'ObjectId') {
                return res.status(404).send({
                    message: "Please check username, password and try again."
                });
            }
            return res.status(500).send({
                message: "Error retrieving data of the user."
            });
        })
}

exports.updateLogin = (req, res) => {
    var username = req.session.user;
    var password = req.body.password;
    var repeat_password = req.body.repassword;

    if (password == repeat_password) {
        bcrypt.hash(password, 10, function (err, hash) {
            Login.updateOne({
                username: username
            }, {
                password: hash
            }, {
                upsert: true
            }, function (err, start) {
                if (err)
                    return err.message;
                else
                    res.send(200, "password updated");
            })
        });


    }
}