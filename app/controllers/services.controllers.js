const Services = require('../models/services.model.js');

//create and save a new service
exports.create = (req, res) => {
    // validate request
    if (!req.body.title) {
        return res.status(400).send({
            message: "Contents cannot be empty"
        });
    }
    var numServices = req.body.numServices;
    var serviceObj =[];
    console.log(req.body);
    for (var i = 0; i < numServices; i++) {
        let imageId = i;
        let otherId = i+1;
        
        let serviceTitle = req.body["title_"+otherId];
        let serviceDescription = req.body["description_"+otherId];
        let serviceImage = req.files[imageId].filename;
        // imageObj.push(req.files[i].originalname);
        serviceObj.push({
            title: serviceTitle,
            description: serviceDescription,
            image: serviceImage
        });
    }
    // creating a service
    const serv = new Services({
        title: req.body.title || "Untitled Content",
        heading: req.body.heading,
        serviceID: "ARCHY-" + Math.floor(Math.random()),
        services: serviceObj
    });
    // console.log(req.files);
    // save content in the database
    serv.save()
        .then(data => {
            res.redirect("/dashboard");
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred"
            });
        });
};

// retrieve services
exports.findAll = (req, res) => {
    Services.find()
        .then(service => {
            res.send(service);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred."
            });
        });
};

// finds a single service with id
exports.findOne = (req, res) => {
    Services.findById(req.params.serviceId)
        .then(service => {
            if (!service) {
                return res.status(404).send({
                    message: "No service found with id:" + req.params.serviceId
                });
            }
            res.send(service);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "No service found with id: " + req.params.serviceId
                });
            }
            return res.status(500).send({
                message: "Error retrieving service with id: " + req.params.serviceId
            });
        });
};

//deletes a service
exports.delete = (req, res) => {
    var serviceID = req.params.serviceId;
    Services.deleteOne({
        _id: serviceID
    }, function (err, res) {});

    res.json({
        success: serviceID,
        status: '1 record deleted'
    });
};

//updates the service
exports.update = (req, res) => {
    var numServices = req.body.numServicesEdit; 
    var sid = req.body.serviceID;
    console.log(sid+" , "+numServices);
    if (!req.body.edit_title) {
        return res.status(400).send({
            message: "Contents cannot be empty"
        });
    }
    var serviceObj =[];
    // var files=req.files;
    // console.log(req.body);
    // console.log(req.files);
    for (var i = 0; i < numServices; i++) {
        var otherId =i+1;
        var serviceTitle = req.body["edit_title_"+otherId];
        var serviceDescription = req.body["edit_description_"+otherId];
        serviceObj.push({
            title: serviceTitle,
            description: serviceDescription,
            image: req.body["edit_image_"+otherId]
        });
    }

    //image entry
    for(var x=0; x<numServices; x++){
        let id=x;
        if(req.files[id]!=undefined){
            let fieldnum=req.files[id].fieldname.match(/(\d+)/)[0];
            console.log(fieldnum);
            serviceObj[fieldnum-1].image=req.files[id].filename;
        }
    }
    //console.log(serviceObj);
    
    Services.updateOne({
            _id: sid
        }, {
            $set : {
                title: req.body.edit_title || "Untitled Content",
                heading: req.body.edit_heading,
                serviceID: "ARCHY-" + Math.floor(Math.random()),
                services: serviceObj
            }
        }, {
            upsert: true
        },
        function (err, stat) {
            if (err)
                return err.message;

            res.redirect("/dashboard");
    });
}