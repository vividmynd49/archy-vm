const Intro = require('../models/intro.model.js');

//create and save a new service
exports.create = (req, res) => {
    // validate request
    // console.log(req.body.content);
    if (!req.body.content) {
        return res.status(400).send({
            message: "Contents cannot be empty"
        });
    }
    // creating an update
    const intros = new Intro({
        content: JSON.parse(req.body.content)
    });
    // save content in the database
    intros.save().then(data => {
        res.send("200", "OK");
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred"
        });
    });
};

// retrieve feedbacks
exports.findAll = (req, res) => {
    Intro.find()
        .then(content => {
            res.send(content);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred."
            });
        });
};

// update feedbacks
exports.modifyOne = (req, res)=>{
    let introID = req.body.introId;
    // creating an update
    let content = JSON.parse(req.body.content);

    Intro.updateOne({
        _id: introID
    },
    {
        content: content
    },{
        upsert: true
    },
    function(err, start){
        if(err)
            return err.message; 
        else{
            res.send(200, "record updated");
        }
    })
}