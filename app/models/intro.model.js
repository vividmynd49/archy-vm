const mongoose=require('mongoose');
const introSchema=mongoose.Schema({
   content: Array
}, {
    timestamps: true
});

module.exports=mongoose.model('Intro', introSchema);