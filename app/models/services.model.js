const mongoose=require('mongoose');
const serviceSchema=mongoose.Schema({
    title: String,
    heading: String,
    serviceID: String,
    services: Object
}, {
    timestamps: true
});

module.exports=mongoose.model('Services', serviceSchema);