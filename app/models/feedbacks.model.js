const mongoose=require('mongoose');
const feedbackSchema=mongoose.Schema({
   content: Array
}, {
    timestamps: true
});

module.exports=mongoose.model('Feedback', feedbackSchema);