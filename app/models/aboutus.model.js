const mongoose=require('mongoose');
const aboutusSchema=mongoose.Schema({
   content: String
}, {
    timestamps: true
});

module.exports=mongoose.model('About', aboutusSchema);