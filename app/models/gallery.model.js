const mongoose=require('mongoose');
const gallerySchema=mongoose.Schema({
    title: String,
    category: String,
    date: Date,
    content: String,
    galleryID: String,
    images:Array
}, {
    timestamps: true
});

module.exports=mongoose.model('Gallery', gallerySchema);