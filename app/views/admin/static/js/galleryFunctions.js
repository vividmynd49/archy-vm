new Vue({
    el:"#app", 
    data:{
      allStoryList:'',
      openedStoryList:'',
      imageDeletionArray:[]
    },
    mounted:function(){
      this.GetGallery();
    },
    methods:{
        // service related functions : start
        GetGallery:function(){
            $.ajax({
                url:"/gallery",
                type:"GET",
                contentType:"application/json"
            }).done(res=>{
                this.allStoryList=res;
            }).fail(err=>{
                console.log(err.message);
            });
        },
        DeleteStory:function(id){
            $.ajax({
                url: "/gallery/"+id,
                type: "DELETE",
                contentType: "application/json"
            }).done(res=>{
                console.log(res);
                this.allStoryList="";
                this.GetGallery();
            }).fail(err=>{
                console.log(err.message);   
            });
        },
        ModifyStory:function(id){
            if(this.imageDeletionArray.length>0){
                this.openedStoryList.images=this.openedStoryList.images.filter( ( el ) => !this.imageDeletionArray.includes( el ) );
            }
            let obj=JSON.stringify( {
                "title": this.openedStoryList.title,
                "content": this.openedStoryList.content,
                "images": this.openedStoryList.images,
                "fordeletion": this.imageDeletionArray
            });
            $.ajax({
                url: "/gallery/"+id,
                type: "PUT",
                contentType: "application/json",
                data: obj
            }).done(res=>{
                this.imageDeletionArray=[];
                $("#edit-gallery-modal").modal('toggle');
            }).fail(err=>{
                console.log(err);
            });
        },
        GetStoryById:function(id){
            $.ajax({
                url:"/gallery/"+id,
                type:"GET",
                contentType:"application/json"
            }).done(res=>{
                this.openedStoryList=res;
                $("#edit-gallery-modal").modal("toggle");
            }).fail(err=>{
                console.log(err.message);
            });
        },
        AddToImageArray: function(img, event){
            this.imageDeletionArray.push(img);
            $(event.target).addClass("hide");
            $(event.target).parent().find(".unmark-overlay").removeClass("hide");
        },
        RemoveFromImageArray: function(img, event){
            this.imageDeletionArray.splice(this.imageDeletionArray.indexOf(img), 1);
            $(event.target).addClass("hide");
            $(event.target).parent().find(".deletion-overlay").removeClass("hide");
        }
        // service related functions : end   
    }
  });