//component description
var navbar={
  template: `<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <a class="navbar-brand brand-logo" href="/dashboard" style='font-weight: 600'>archyevents</a>
    <a class="navbar-brand brand-logo-mini" href="/dashboard" style='font-weight: 600'>archyevents</a>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-stretch">
    
    <ul class="navbar-nav navbar-nav-right">
      
      <li class="nav-item d-none d-lg-block full-screen-link">
        <a class="nav-link">
          <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
        </a>
      </li>
      
      
      <li class="nav-item nav-logout d-none d-lg-block">
        <a class="nav-link" href="/logout">
          <i class="mdi mdi-power"></i>
        </a>
      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="mdi mdi-menu"></span>
    </button>
  </div>
</nav>`
};
var sidebar={
      template:`<nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="./dashboard">
            <span class="menu-title">Services</span>
            <i class="mdi mdi-creation menu-icon"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./gallery-content">
            <span class="menu-title">Gallery</span>
            <i class="mdi mdi-image-area menu-icon"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./intro-content">
            <span class="menu-title">Intros</span>
            <i class="mdi mdi-summit menu-icon"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./feedback-content">
            <span class="menu-title">Feedbacks</span>
            <i class="mdi mdi-comment-account menu-icon"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./aboutus-content">
            <span class="menu-title">About</span>
            <i class="mdi mdi-information menu-icon"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./settings">
            <span class="menu-title">Settings</span>
            <i class="mdi mdi-settings  menu-icon"></i>
          </a>
        </li>
        
      </ul>
    </nav>`
};
var footer={
  template:`
        <div class="d-sm-flex justify-content-center justify-content-sm-between footer">
          <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Developed by <a href="https://vividmynd.in">VIVID MYND</a></span>
        </div>
  `
};
//component registration
Vue.component('navbar', navbar);
Vue.component('sidebar', sidebar);
Vue.component('foot', footer);