new Vue({
    el:"#app", 
    data:{
      introContent:''
    },
    mounted: function(){
        this.GetIntro();
    },
    methods:{
        SaveIntro: function () {
            var obj=[];
            for(var i=0; i<3; i++){
                var count=i+1;
                var object={
                    "heading": $("#heading"+count).val(),
                    "content": $("#content"+count).val()
                }
                obj.push(object);
            }
            $.ajax({
                url: "/addintro",
                type: "post",
                data: {
                    "content": JSON.stringify(obj) 
                }
            }).done((res)=>{
                if(res=="OK") 
                    this.GetIntro();
                else
                    alert("Unable to save intro");
            }).fail(err=>{
                console.log(err.message);
            })
        },
        GetIntro: function(){
            $.ajax({
                url: "/getintro",
                type: "GET",
                contentType: "application/json"
            }).done((res)=>{
                let content=res[0];
                this.introContent=content;
                for(let i=0; i<3; i++){
                    let count=i+1;
                    $("#heading"+count).val(this.introContent.content[i].heading);
                    $("#content"+count).val(this.introContent.content[i].content);
                }
            }).fail((err)=>{
                console.log(err.message);
            })
        },
        UpdateIntro: function(id){
            var obj=[];
            for(var i=0; i<3; i++){
                var count=i+1;
                var object={
                    "heading": $("#heading"+count).val(),
                    "content": $("#content"+count).val()
                }
                obj.push(object);
            }
            $.ajax({
                url: "/updateintro",
                type: "PUT",
                data: {
                    "introId": id,
                    "content": JSON.stringify(obj)
                }
            }).done((res)=>{
                console.log(res);
                this.GetIntro();
            }).fail((err)=>{
                console.log(err.message);
            })
        }
    }
  });