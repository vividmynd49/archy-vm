module.exports = (app) => {
    const feedbacks = require("../controllers/feedbacks.controllers.js");

    app.post("/addfeedbacks", feedbacks.create);
    app.get("/getfeedbacks", feedbacks.findAll);
    app.get("/updatefeedbacks", feedbacks.modifyOne);
}