new Vue({
    el:"#app", 
    data:{
      aboutUsContent:''
    },
    mounted: function(){
        this.GetUpdate();
    },
    methods:{
        SaveUpdate: function () {
            let content=$("#texteditable").html();
            if(content.length!=0){
                $.ajax({
                    url: "/addaboutus",
                    type: "post",
                    data: {
                        "content": content
                    }
                }).done((res)=>{
                    if(res=="OK"){
                        console.log("About us updated");
                        window.location.href="./dashboard";
                    }else{
                        console.log("Failed");
                    }
                }).fail((err)=>{
                    console.log(err.message);
                })
            }else{
                alert("Content can't be blank");
            }
        },
        GetUpdate: function(){
            $.ajax({
                url: "/getaboutus",
                type: "GET",
                contentType: "application/json"
            }).done((res)=>{
                let len=res.length;
                let content=res[len-1].content;
                $("#texteditable").html(content);
            }).fail((err)=>{
                console.log(err.message);
            })
        }
    }
  });