new Vue({
    el:"#app", 
    data:{
      feedbackContent:''
    },
    mounted: function(){
        this.GetFeedback();
    },
    methods:{
        SaveFeedback: function () {
            var obj=[];
            for(var i=0; i<3; i++){
                var count=i+1;
                var object={
                    "gender": $("#gender"+count).val(),
                    "name": $("#name"+count).val(),
                    "feedback": $("#feedback"+count).val()
                }
                obj.push(object);
            }
            $.ajax({
                url: "/addfeedbacks",
                type: "post",
                data: {
                    "content": JSON.stringify(obj) 
                }
            }).done((res)=>{
                if(res=="OK") 
                    this.GetFeedback();
                else
                    alert("Unable to save feedbacks");
            }).fail(err=>{
                console.log(err.message);
            })
        },
        GetFeedback: function(){
            $.ajax({
                url: "/getfeedbacks",
                type: "GET",
                contentType: "application/json"
            }).done((res)=>{
                let content=res[0];
                this.feedbackContent=content;
                for(let i=0; i<3; i++){
                    let count=i+1;
                    $("#gender"+count).val(this.feedbackContent.content[i].gender);
                    $("#name"+count).val(this.feedbackContent.content[i].name);
                    $("#feedback"+count).val(this.feedbackContent.content[i].feedback);
                }
            }).fail((err)=>{
                console.log(err.message);
            })
        },
        UpdateFeedback: function(id){
            var obj=[];
            for(var i=0; i<3; i++){
                var count=i+1;
                var object={
                    "gender": $("#gender"+count).val(),
                    "name": $("#name"+count).val(),
                    "feedback": $("#feedback"+count).val()
                }
                obj.push(object);
            }
            $.ajax({
                url: "/updatefeedbacks",
                type: "PUT",
                data: {
                    "feedbackId": id,
                    "content": JSON.stringify(obj)
                }
            }).done((res)=>{
                console.log(res);
                this.GetFeedback();
            }).fail((err)=>{
                console.log(err.message);
            })
        }
    }
  });