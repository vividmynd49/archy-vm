new Vue({
    el: "#app",
    data: {
        username: '',
        password: ''
    },
    methods: {
        Login: function(){
            $.ajax({
                url: "/login",
                type: "POST",
                data: {
                    "username": this.username,
                    "password": this.password 
                }
            }).done(res=>{
                console.log(res);
                if(res=="OK"){
                    location.href="/dashboard";
                }else{
                    $("#invalid-cred").removeClass("hide");
                }
            }).fail(res=>{
                $("#invalid-cred").removeClass("hide");
            });
        },
        Update: function(){
            $.ajax({
                url: "/update-login",
                type: "put",
                data: {
                    "password": $("#new-password").val(),
                    "repassword": $("#repeat-password").val()
                }
            }).done(res=>{
                if(res=="OK")
                    location.href="/logout";
                
            }).fail(err=>{
                console.log(err.message);
            })
        }
    }
});