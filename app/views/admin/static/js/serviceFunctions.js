new Vue({
    el:"#app", 
    data:{
      username: '',
      password: '',
      allServiceList:'',
      openedServiceList:'',
      imageDeletionArray:[]
    },
    mounted:function(){
      this.GetService();
    },
    methods:{
        Login: function () {
          let obj = JSON.stringify({
              "username": this.username,
              "password": this.password
          });
          $.ajax({
              url: "/login",
              type: "post",
              data: obj,
              contentType: "application/json"
          }).done((success) => {
              if (success == "OK")
                  location.href = "/dashboard";
              else
                  alert("Wrong credentials. Please try again.");
          }).fail(err => {
              console.log("Something went wrong.");
          })
        },
        
        // service related functions : start
        GetService:function(){
            $.ajax({
                url:"/services",
                type:"GET",
                contentType:"application/json"
            }).done(res=>{
                this.allServiceList=res;
            }).fail(err=>{
                console.log(err.message);
            });
        },
        DeleteService:function(id){
            $.ajax({
                url: "/services/"+id,
                type: "DELETE",
                contentType: "application/json"
            }).done(res=>{
                console.log(res);
                this.allServiceList="";
                this.GetService();
            }).fail(

            );
        },
        AddToImageArray: function(img){
            this.imageDeletionArray.push(img);
        },
        RemoveFromImageArray: function(img){
            this.imageDeletionArray.splice( this.imageDeletionArray.indexOf(img), 1 );
        },
        GetServiceById:function(id){
          $.ajax({
              url:"/services/"+id,
              type:"GET",
              contentType:"application/json"
          }).done(res=>{
              this.openedServiceList=res;
              var services = res.services;
              $(".edit-service-list").html("");
              $("#num-of-services-to-edit").val(services.length);
              $("#serviceID").val(res._id);
              for(var id=0; id<services.length; id++){
                var otherid=id+1;
                $(".edit-service-list").append(`<fieldset>
                <img src='/appimages/`+services[id].image+`' class="custom-card-image mb-2">
                <br/>
                <input type="text" placeholder="Service Title" class="form-control custom-textfield" value="`+services[id].title+`" name="edit_title_`+otherid+`" >
                <textarea type="text" placeholder="Description" class="form-control custom-textarea" name="edit_description_`+otherid+`">`+services[id].description+`</textarea>
                <small>Change image</small>
                <input type="file" id='service_image`+otherid+`' class="form-control custom-textfield edit_service_image" name="edit_service_image`+otherid+`">
                <input type="hidden" value="`+services[id].image+`" name="edit_image_`+otherid+`">
                </fieldset>
                <br/>`);
              }
              $("#edit-service-modal").modal('show');
          }).fail(err=>{
              console.log(err.message);
          });
        },
        AddNewService: function(mode){
            let parent;
            let child="fieldset";
            switch(mode){
                case "add": {
                    parent = ".add-service-details";
                    break;
                }
                case "edit": {
                    parent = ".edit-service-list";
                    break;
                }
            }
            if(mode=="add"){
                const children = $(parent+" "+child).length;
                let id=children+1;
                $("#num-of-services").val(id);
                $(parent).append(`<fieldset>
                <input type="text" placeholder="Service Title" class="form-control custom-textfield" name="title_`+id+`" >
                <textarea type="text" placeholder="Description" class="form-control custom-textfield" name="description_`+id+`"></textarea>
                <small>Choose image</small>
                <input type="file" id='service_image`+id+`' class="form-control custom-textfield" name="service_image">
                <!--p>
                     <i class="mdi mdi-delete-circle delete-icon"></i>
                 </p-->
                </fieldset>
                <br/>`);
            }
            if(mode=="edit"){
                const children = $(parent+" "+child).length;
                let id=children+1;
                $("#num-of-services-to-edit").val(id);

                $(parent).append(`<fieldset>
                <input type="text" placeholder="Service Title" class="form-control custom-textfield" name="edit_title_`+id+`" >
                <textarea type="text" placeholder="Description" class="form-control custom-textarea" name="edit_description_`+id+`"></textarea>
                <small>Add image</small>
                <input type="file" id='service_image`+id+`' class="form-control custom-textfield edit_service_image" name="edit_service_image`+id+`">
                <!--p>
                 <i class="mdi mdi-delete-circle delete-icon" onclick="RemoveParent(`+id+`, 'edit');"></i>
                 </p-->
                </fieldset>
                <br/>`);
            }
        }
    }
  });

  function RemoveParent(id, mode){
        if(mode=="edit")
            $(".edit-service-list fieldset:nth-child("+id+")").remove();
        if(mode=="add")
            $(".add-service-details fieldset:nth-child("+id+")").remove();
  }