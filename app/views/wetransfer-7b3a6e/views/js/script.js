
document.addEventListener('readystatechange', event => {
    if (event.target.readyState === 'complete') {
        // header
        let hamBurger = document.getElementsByClassName('hamburger')[0];
        hamBurger.addEventListener('click', function () {
            let navBar = document.getElementsByTagName('nav')[0];
            if(navBar.classList.value === 'active'){
                $('nav').slideUp();
                navBar.classList.toggle('active');
            }else{
                $('nav').slideDown();
                navBar.classList.toggle('active')
            }
        });
        // header ends
        // this event listner closes the enquiry pop-up
        let closeBtn = document.getElementsByClassName('cross')[0];
        closeBtn.addEventListener('click', function () {
            let enquiryPopUp = document.getElementById('enquiryPopUp');
            enquiryPopUp.classList.add("hide");
        });

    }
})

$(".contact-btn").on('click', ()=>{
    $("#enquiryPopUp").removeClass("hide");
});

function loadServiceList(){
    $.ajax({
        url: "/services",
        type: "get"
    }).done(res => {
        for(var i=0; i<res.length; i++) {
            $(".serviceList").append("<li><a href='/event/"+res[i]._id+"'>"+res[i].title+"</a></li>");
        }
    }).fail(err => {
        console.log(err);
    });
}