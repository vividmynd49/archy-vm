document.addEventListener("readystatechange", event => {
  if (event.target.readyState === "complete") {
    // bla bla bla
    let imageOperation = {
      prevImage: function(e) {
        if (e.target !== e.currentTarget) {
          let imageContainer = document.getElementsByClassName("prevImg")[0];
          imageContainer.src = e.target.src;
          let displaySection = document.getElementById("prevImgContainer");
          displaySection.style.display = "flex";
        }
        e.stopPropagation();
      }
    };

    // batauga kabhi
    let imgElement = document.querySelector("#imgConatiner");
    if (imgElement) {
      imgElement.addEventListener("click", imageOperation.prevImage, false);
    }

    // closes the section
    let selector = document.getElementsByClassName("closeSec")[0];
    if(selector){
    selector.addEventListener("click", function() {
      document.getElementById("prevImgContainer").style.display = "none";
    });
}
  }
});
